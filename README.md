# PROCEDIMENTO DE INSTALAÇÃO DO SEI COM VAGRANT
Este procedimento permite a instalação do SEI nos ambientes de desenvolvimento com módulos em docker para os Órgãos do Estado de Alagoas.

Alguns links com documentações importantes:

*    [Documentação Técnica do SEI](https://softwarepublico.gov.br/social/sei/manuais/documentacao-de-apoio)
*    [Padrão de Codificação PHP](https://softwarepublico.gov.br/social/sei/manuais/padrao-de-codificacao-php/sumario)
*    [Padrão de Modelagem de Dados](https://softwarepublico.gov.br/social/sei/manuais/padrao-de-modelagem-de-dados/sumario)

### 01. Instalação do Vagrant e Dependências:

O Vagrant é uma ferramenta que auxilia na criação de máquinas virtuais para a infraestrutura do seu projeto. O Vagrant depende do um virtualizador,
como o VirtualBox ou VMWare, neste exemplo utilizaremos o VirtualBox.

Instalação do VirtualBox:

    https://www.virtualbox.org/wiki/Downloads

Após a Instalação do Virtualbox, agora é hora de instalar o Vagrant:
    
    https://www.vagrantup.com/downloads.html
  
Instalação do Git:
    
    https://git-scm.com/downloads
    
### 02. Acesse via terminal o diretório onde irá ficar os arquivos do SEI: 
Execute o seguinte comando:

    git clone https://victorsls@bitbucket.org/nds_seplag/sei.git

Após a execução do comando, dentro do diretório do SEI deve conter os seguintes diretórios:

    /sei
    /sip
    /infra

Os arquivos serão utilizados pelo Vagrant para a ativação do sistema dentro das máquinas virtuais. 
Todas as modificações feitas nos arquivos PHP, será refletida de forma automática no ambiente.

Para acessar o ambiente:

	http://localhost/sei

### 03. Configuração Inicial do Vagrant 
Dentro do diretório do SEI, execute o seguinte comando:

	vagrant init processoeletronico/sei-3.0.0

O comando irá criar um arquivo chamado Vagrantfile dentro do diretório atual, esse arquivo é a configuração inicial do Vagrant contendo o Box do SEI.

### 04. Iniciar o Ambiente Vagrant
Dentro do diretório do SEI, execute o comando abaixo:

    vagrant up

No linux é necessário executar o comando como sudo, para conseguir utilizar a porta 80. 
    
    sudo vagrant up

Esse comando irá iniciar a construção do ambiente de desenvolvimento começando pelo download da Box processoeletronico/sei-3.0.0 contendo todo o ambiente preparado para o desenvolvimento.

É normal que a primeira execução desse comando demore vários minutos para ser concluído, pois a imagem/box, com cerca de 2GB, será baixada para a máquina de desenvolvimento. Após o fim da transferência, o ambiente estará disponível em questão de minutos.

Após a conclusão do primeiro provisionamento, o ambiente poderá ser destruído e recriado rapidamente já que vagrant armazenará a BOX/Imagem de referência em seu cache.

Ao final da inicialização do ambiente de desenvolvimento, será apresentada a mensagem abaixo, indicando que todos os serviços do SEI já estão em operação na máquina de desenvolvimento:
    
    ===> default: Starting smtp
    ===> default: Starting jod
    ===> default: Starting mysql
    ===> default: Starting solr
    ===> default: Starting memcached    
    ===> default: Starting httpd
    
## INFORMAÇÕES ADICIONAIS

#### Comandos Básicos do Vagrant
Os seguintes comandos poderão ser executados no diretório onde está localizado o código-fonte do SEI para ligar e desligar o ambiente:

    vagrant up

Responsável por inicializar as máquina virtuais e gerenciar o redirecionamento dos serviços para VM para à máquina local de desenvolvimento. Caso não exista nenhum ambiente previamente configurado na máquina local, ele também se encarregará de fazer toda a instalação e provisionamento da máquina virtual.

    vagrant halt

Responsável por desligar as máquina virtuais desligar todos os serviços em execução.

    vagrant reload

Reinicia todo o ambiente de desenvolvimento e sua máquina virtual.

    vagrant destroy

Como o nome diz, destrói todo o ambiente de desenvolvimento utilizado até o momento, apagando todas as informações persistidas no banco de dados. A simples execução do comando vagrant up reconstrói um novo ambiente limpo para o sistema SEI.

    vagrant box update